import explotaciones.Explotacion;
import explotaciones.ParcelaDeAlmendros;
import explotaciones.ParcelaDeOlivar;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        ParcelaDeAlmendros pa1 = new ParcelaDeAlmendros();
        pa1.setNombreDeLaParcela("Parcela 1 de almendros");
        pa1.setNumeroPiesDeArbol(100);
        pa1.setGastosTotalesEnParcela(500);
        pa1.setEscandallo(0.240);
        pa1.setKilosDeAlmendra(1000);
        pa1.setPrecioAlmendraLimpia(4);

        ParcelaDeOlivar po1 = new ParcelaDeOlivar();
        po1.setNombreDeLaParcela("Parcela de olivar");
        po1.setNumeroPiesDeArbol(600);
        po1.setArrobasDeAceite(100);
        po1.setGastosTotalesEnParcela(600);
        po1.setPrecioArroba(36);

        ParcelaDeAlmendros pa2 = new ParcelaDeAlmendros();
        pa2.setNombreDeLaParcela("Parcela 2 de almendros");
        pa2.setNumeroPiesDeArbol(150);
        pa2.setGastosTotalesEnParcela(700);
        pa2.setEscandallo(0.225);
        pa2.setKilosDeAlmendra(1450);
        pa2.setPrecioAlmendraLimpia(4.25);

        Explotacion explotacion = new Explotacion();
        explotacion.annadirParcela(pa1);
        explotacion.annadirParcela(po1);
        explotacion.annadirParcela(pa2);
        System.out.println(explotacion);
    }
}