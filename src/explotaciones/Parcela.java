package explotaciones;

public abstract class Parcela {
    protected String nombreDeLaParcela;
    protected int numeroPiesDeArbol;
    protected double ingresosTotalesDeLaParcela;
    protected double gastosTotalesEnParcela;
    protected double beneficioTotalEnParcela;

    public Parcela(String nombreDeLaParcela, int numeroPiesDeArbol, double ingresosTotalesDeLaParcela, double gastosTotalesEnParcela, double beneficioTotalEnParcela) {
        this.nombreDeLaParcela = nombreDeLaParcela;
        this.numeroPiesDeArbol = numeroPiesDeArbol;
        this.ingresosTotalesDeLaParcela = ingresosTotalesDeLaParcela;
        this.gastosTotalesEnParcela = gastosTotalesEnParcela;
        this.beneficioTotalEnParcela = beneficioTotalEnParcela;
    }

    public Parcela(){

    }

    @Override
    public String toString() {
        return nombreDeLaParcela + "\n" +
                "---------------\n" +
                "Número de pies de árbol: " + numeroPiesDeArbol + "\n" +
                "Ingresos totales de la parcela: " + ingresosTotalesDeLaParcela() + " €" + "\n" +
                "Gastos totales en la parcela: " + gastosTotalesEnParcela + " €" + "\n" +
                "Beneficios de la parcela: " + beneficioTotalEnParcela() + " €" + "\n";
    }

    public abstract double beneficioTotalEnParcela();
    public abstract double ingresosTotalesDeLaParcela();

    public void setNombreDeLaParcela(String nombreDeLaParcela) {
        this.nombreDeLaParcela = nombreDeLaParcela;
    }

    public void setNumeroPiesDeArbol(int numeroPiesDeArbol) {
        this.numeroPiesDeArbol = numeroPiesDeArbol;
    }

    public void setGastosTotalesEnParcela(double gastosTotalesEnParcela) {
        this.gastosTotalesEnParcela = gastosTotalesEnParcela;
    }

}

