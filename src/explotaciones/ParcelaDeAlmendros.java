package explotaciones;

public class ParcelaDeAlmendros extends Parcela {
    private double kilosDeAlmendra;
    private double escandallo;
    private double precioAlmendraLimpia;

    private ParcelaDeAlmendros(String nombreDeLaParcela, int numeroPiesDeArbol, double ingresosTotalesDeLaParcela, double gastosTotalesEnParcela, double beneficioTotalEnParcela, double kilosDeAlmendra, double escandallo, double precioAlmendraLimpia) {
        super(nombreDeLaParcela, numeroPiesDeArbol, ingresosTotalesDeLaParcela, gastosTotalesEnParcela, beneficioTotalEnParcela);
        this.kilosDeAlmendra = kilosDeAlmendra;
        this.escandallo = escandallo;
        this.precioAlmendraLimpia = precioAlmendraLimpia;
    }

    public ParcelaDeAlmendros() {

    }

    public double comprobarEscandallo() {
        if (escandallo >= 0 && escandallo<=1)
            return escandallo;
        return 0;
    }

    @Override
    public double beneficioTotalEnParcela() {
        return (this.kilosDeAlmendra * this.escandallo * this.precioAlmendraLimpia) - this.gastosTotalesEnParcela;
    }

    public double ingresosTotalesDeLaParcela() {
        return (this.kilosDeAlmendra * this.escandallo * precioAlmendraLimpia);
    }

    @Override
    public String toString() {
        return super.toString() +
                "Kilos de almendra: " + kilosDeAlmendra + "\n" +
                "Escandallo: " + comprobarEscandallo() + "\n" +
                "Precio de la almendra limpia: " + precioAlmendraLimpia + " €" + "\n\n";
    }

    public void setKilosDeAlmendra(double kilosDeAlmendra) {
        this.kilosDeAlmendra = kilosDeAlmendra;
    }

    public void setEscandallo(double escandallo) {
        this.escandallo = escandallo;
    }

    public void setPrecioAlmendraLimpia(double precioAlmendraLimpia){
        this.precioAlmendraLimpia = precioAlmendraLimpia;

    }
}

