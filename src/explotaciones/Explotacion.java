package explotaciones;

import java.util.ArrayList;

public class Explotacion {
    ArrayList<Parcela> explotacion = new ArrayList<>();

    public Explotacion() {
    }

    public void annadirParcela(Parcela parcela) {
        explotacion.add(parcela);
    }

    public double calcularBeneficioTotalExplotacion(){
        double suma = 0;
        for(Parcela parcela : explotacion)
            suma += parcela.beneficioTotalEnParcela();
        return suma;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (explotaciones.Parcela parcela : explotacion) {
            sb.append(parcela + "\n");
        }
        return sb.toString() + "LOS BENEFICIOS TOTALES DE LA EXPLOTACIÓN HAN SIDO: " + calcularBeneficioTotalExplotacion() + " €";
    }

}